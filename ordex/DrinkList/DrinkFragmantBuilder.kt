package com.hungrymedia.orderex.ui.drinkList

import androidx.lifecycle.ViewModel
import com.infullmobile.mcintosh.inject.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class DrinkFragmantBuilder {


    @ContributesAndroidInjector
    internal abstract fun fragment(): DrinksFragment

    @Binds
    @IntoMap
    @ViewModelKey(DrinkViewModel::class)
    abstract fun bindViewModel(viewModel: DrinkViewModel): ViewModel

}