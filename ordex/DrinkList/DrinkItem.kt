package com.hungrymedia.orderex.ui.drinkList

import android.os.Parcelable
import com.hungrymedia.orderex.ui.size.SizeOrderItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DrinkItem(
    val id: Int,
    val drinkImage: Int,
    val drinkName: String,
    val drinkCost: Double,
    val sizeDrink: MutableList<SizeOrderItem>
) : Parcelable