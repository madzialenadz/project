package com.hungrymedia.orderex.ui.drinkList

interface OrderDrinkRestaurantListener {
    fun orderDrinkPosition(drink: DrinkItem)
}