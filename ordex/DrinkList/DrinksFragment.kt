package com.hungrymedia.orderex.ui.drinkList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.hungrymedia.orderex.R
import com.hungrymedia.orderex.ui.base.BaseFragment
import com.hungrymedia.orderex.ui.activity.home.HomeActivity
import kotlinx.android.synthetic.main.drink_fragment.*

class DrinksFragment : BaseFragment(), OrderDrinkRestaurantListener {

    private lateinit var detailsAdapter: DrinkAdapter

    override val screenTitle: Int
        get() = R.string.drinks

    companion object {
        fun newInstance(): DrinksFragment {
            return DrinksFragment()
        }
    }

    private val viewModel: DrinkViewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(DrinkViewModel::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.drink_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecycleView()
    }

    private fun setRecycleView() {
        detailsAdapter = DrinkAdapter()
        detailsAdapter.setClickOrderDrink(this)
        drink_list.apply {
            layoutManager = GridLayoutManager(context, 2)
            (layoutManager as GridLayoutManager).requestLayout()
            adapter = detailsAdapter
        }
        viewModel.drinkList.observe(viewLifecycleOwner, Observer {
            detailsAdapter.data = it

        })
    }

    override fun orderDrinkPosition(drink: DrinkItem) {
        (activity as HomeActivity).navigateToOrderDrinkDetails(drink)
    }
}