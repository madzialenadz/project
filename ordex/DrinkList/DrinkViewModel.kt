package com.hungrymedia.orderex.ui.drinkList

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hungrymedia.orderex.R
import com.hungrymedia.orderex.ui.size.SizeOrderItem
import javax.inject.Inject

class DrinkViewModel @Inject constructor(private val applicationContext: Context) : ViewModel() {

    val drinkList: MutableLiveData<MutableList<DrinkItem>> = MutableLiveData()

    init {
        showDrinkList()
    }

    private fun showDrinkList() {
        val showDrinkList: MutableList<DrinkItem> = mutableListOf()

        val beersSizeList: MutableList<SizeOrderItem> = mutableListOf()
        beersSizeList.add(SizeOrderItem(1, "S", "Small", 4.00))
        beersSizeList.add(SizeOrderItem(2, "M", "Medium", 5.00))
        beersSizeList.add(SizeOrderItem(3, "L", "Large", 6.50))

        val cocaSizeList: MutableList<SizeOrderItem> = mutableListOf()
        cocaSizeList.add(SizeOrderItem(1, "S", "Small", 2.50))
        cocaSizeList.add(SizeOrderItem(2, "M", "Medium", 4.00))
        cocaSizeList.add(SizeOrderItem(3, "L", "Large", 6.50))

        val pepsiSizeList: MutableList<SizeOrderItem> = mutableListOf()
        pepsiSizeList.add(SizeOrderItem(1, "S", "Small", 2.50))
        pepsiSizeList.add(SizeOrderItem(2, "M", "Medium", 3.00))
        pepsiSizeList.add(SizeOrderItem(3, "L", "Large", 4.50))

        val spriteSizeList: MutableList<SizeOrderItem> = mutableListOf()
        spriteSizeList.add(SizeOrderItem(1, "S", "Small", 3.00))
        spriteSizeList.add(SizeOrderItem(2, "M", "Medium", 4.00))
        spriteSizeList.add(SizeOrderItem(3, "L", "Large", 5.50))

        showDrinkList.add(DrinkItem(1, R.drawable.beers, "Beers", 4.00, beersSizeList))
        showDrinkList.add(DrinkItem(2, R.drawable.coca, "Coca", 2.50, cocaSizeList))
        showDrinkList.add(DrinkItem(3, R.drawable.pepsi_cola, "Pepsi", 2.50, pepsiSizeList))
        showDrinkList.add(DrinkItem(4, R.drawable.sprite_cola, "Sprite", 3.00, spriteSizeList))


        drinkList.postValue(showDrinkList)
    }
}