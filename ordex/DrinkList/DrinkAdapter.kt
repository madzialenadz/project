package com.hungrymedia.orderex.ui.drinkList

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hungrymedia.orderex.R
import com.hungrymedia.orderex.ui.base.BaseAdapter
import kotlinx.android.synthetic.main.item_drink.view.*
import java.lang.ref.WeakReference
import java.text.DecimalFormat

class DrinkAdapter : BaseAdapter<DrinkItem, DrinkAdapter.DrinkViewHolder>() {


    var listenSelectOrderDrink: WeakReference<OrderDrinkRestaurantListener>? = null

    fun setClickOrderDrink(listener: OrderDrinkRestaurantListener) {
        this.listenSelectOrderDrink = WeakReference(listener)
    }


    override fun onBindViewHolder(holder: DrinkViewHolder, item: DrinkItem, position: Int) {
        holder.itemView.apply {

            content_drink_full.setOnClickListener {
                listenSelectOrderDrink?.get()?.orderDrinkPosition(item)
            }
            val precision = DecimalFormat("0.00")
            Glide.with(context).load(item.drinkImage).into(drink_image)
            drink_name.text = item.drinkName
            drink_cost.text =
                String.format(
                    context.getString(
                        R.string.currency_value,
                        precision.format(item.drinkCost).toString()
                    )
                )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder =
        DrinkViewHolder(
            inflate(
                parent,
                R.layout.item_drink
            )
        )


    class DrinkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}