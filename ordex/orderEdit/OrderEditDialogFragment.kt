package com.hungrymedia.orderex.ui.dialog.orderEditDialog

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.hungrymedia.orderex.Constants.ORDER_ITEM
import com.hungrymedia.orderex.Constants.POSITION_ITEM
import com.hungrymedia.orderex.R
import com.hungrymedia.orderex.ui.base.BaseDialogFragment
import com.hungrymedia.orderex.dto.OrderDishDto
import com.hungrymedia.orderex.ui.entree.EntreeItem
import com.hungrymedia.orderex.ui.entree.EntreeDishAdapter
import com.hungrymedia.orderex.ui.side.SideItem
import com.hungrymedia.orderex.ui.side.SideAdapter
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.*
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.add_quantity
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.content_select_entree
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.dish_name
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.remove_quantity
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.side_list
import kotlinx.android.synthetic.main.dialog_order_edit_dish_detail.value_quantity
import org.greenrobot.eventbus.EventBus


class OrderEditDialogFragment : BaseDialogFragment(), View.OnClickListener {

    private lateinit var sideAdapter: SideAdapter
    private lateinit var entreeDishAdapter: EntreeDishAdapter

    private var order: OrderDishDto? = null
    private var position: Int = 0
    private var valueQuantity: Int = 0
    private var dishCost: Double = 0.0
    private var dishTotalCost: Double = 0.0
    private var dishOrderFull: String? = ""

    private var updateOrderDishListener:UpdateOrderDishListener? = null

    companion object {
        fun newInstance(order: OrderDishDto, position: Int): OrderEditDialogFragment {
            val fragment = OrderEditDialogFragment()
            val args = Bundle()
            args.putParcelable(ORDER_ITEM, order)
            args.putInt(POSITION_ITEM, position)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            order = it.getParcelable(ORDER_ITEM)
            position = it.getInt(POSITION_ITEM)
        }
        updateOrderDishListener = targetFragment as UpdateOrderDishListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_order_edit_dish_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecycleView()
        setOnClickListener()
        setEditOrderData(order)

    }

    private fun setOnClickListener() {
        save_order.setOnClickListener(this)
        add_quantity.setOnClickListener(this)
        remove_quantity.setOnClickListener(this)
        content_select_side.setOnClickListener(this)
        content_select_entree.setOnClickListener(this)
    }

    private fun setRecycleView() {
        entreeDishAdapter = EntreeDishAdapter()
        sideAdapter = SideAdapter()


        side_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = sideAdapter
        }

        entree_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = entreeDishAdapter
        }


        val sideList: MutableList<SideItem> = mutableListOf()
        sideList.add(SideItem(1, "Baked Beans", false))
        sideList.add(SideItem(2, "Rhubarb Applesauce", false))
        sideList.add(SideItem(3, "Potato Salad", false))
        sideList.add(SideItem(4, "Creamy Coleslaw", false))

        val showEntreeList: MutableList<EntreeItem> = mutableListOf()
        showEntreeList.add(EntreeItem(1, "3 Ribs", 6.00))
        showEntreeList.add(EntreeItem(2, "3 Wings", 3.75))
        showEntreeList.add(EntreeItem(3, "Fresh Onion Straws", 3.00))
        showEntreeList.add(EntreeItem(4, "Extra side", 2.50))


        sideAdapter.data = sideList
        entreeDishAdapter.data = showEntreeList
    }

    private fun setEditOrderData(order: OrderDishDto?) {

        order?.let {

            it.dishBasicCost?.let { dishOrderCost ->
                dishCost = dishOrderCost
            }


            dish_name.text = it.dishOrderName
            if (it.dishOrderSide != null) {
                dish_extra.text =
                    String.format(getString(R.string.order_extra, it.dishOrderSide))
            }

            if (it.dishOrderExtra != null) {

                val entreeList = entreeDishAdapter.getEntreeItems()

                val entreeSelectedDish: List<String>? =
                    it.dishOrderExtra?.split(" | ")?.map { it.trim() }

                entreeSelectedDish?.toMutableList()?.let {
                    for (entree in entreeList) for (entreeSelected in entreeSelectedDish) {
                        if (entree.entreeDishName == entreeSelected)
                            entree.isSelected = true
                        entreeDishAdapter.notifyDataSetChanged()
                    }
                }

            }

            if (it.dishOrderSide != null) {

                val sideList = sideAdapter.getSlideItem()

                val sideSelectedDish: List<String>? =
                    it.dishOrderSide?.split(" | ")?.map { it.trim() }

                sideSelectedDish?.toMutableList()?.let {

                    for (side in sideList) {
                        for (sideSelected in sideSelectedDish) {
                            if (side.name == sideSelected) {
                                val position = sideAdapter.getSelectedPosition(sideSelected)
                                side.isSelected = true
                                sideAdapter.notifyItemChanged(position)
                            }
                        }
                    }

                }

            }

            value_quantity.text = order.dishOrderQuantity.toString()
            valueQuantity = order.dishOrderQuantity
        }
    }

    override fun onClick(v: View?) {

        val drawable: Drawable?

        when (v?.id) {
            R.id.add_quantity -> {
                displayCurrentQuantity(valueQuantity++)
            }
            R.id.remove_quantity -> {
                if (valueQuantity > 0) {
                    displayCurrentQuantity(valueQuantity--)
                }
            }
            R.id.content_select_side -> {

                if (side_list.visibility == View.VISIBLE) {
                    side_list.visibility = View.GONE
                    drawable = context?.getDrawable(R.drawable.ic_arrow_down)
                } else {
                    side_list.visibility = View.VISIBLE
                    drawable = context?.getDrawable(R.drawable.ic_arrow_up)
                }
                content_select_side.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    drawable,
                    null
                )
            }
            R.id.content_select_entree -> {
                if (entree_list.visibility == View.VISIBLE) {
                    entree_list.visibility = View.GONE
                    drawable = context?.getDrawable(R.drawable.ic_arrow_down)
                } else {
                    entree_list.visibility = View.VISIBLE
                    drawable = context?.getDrawable(R.drawable.ic_arrow_up)
                }

                content_select_entree.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    drawable,
                    null
                )

            }
            R.id.save_order -> {

                val sideItemsOrder = sideAdapter.getSelectedSlideItems()
                val entreeItemsOrder = entreeDishAdapter.getSelectedEntreeItems()
                val entreeItemSelected: MutableList<String> = ArrayList()

                entreeItemsOrder.forEach {
                    entreeItemSelected.add(it.entreeDishName)
                }

                val entreeItemsCost = entreeItemsOrder.sumByDouble {
                    it.entreeDishCost
                }

                val dishOrderDishSide = sideItemsOrder.joinToString(" | ")
                val dishOrderEntree = entreeItemSelected.joinToString(" | ")


                if (dishOrderDishSide.length > 0 && dishOrderEntree.length> 0) {
                    dishOrderFull = "$dishOrderDishSide | $dishOrderEntree"
                } else if ((dishOrderDishSide.length>0 ) && (dishOrderEntree.length ==0)) {
                    dishOrderFull = dishOrderDishSide
                } else if ((dishOrderDishSide.length==0) && (dishOrderEntree.length > 0 )) {
                    dishOrderFull = dishOrderEntree
                } else if (((dishOrderDishSide.length==0) && (dishOrderEntree.length ==0))) {
                    dishOrderFull = null
                }

                dishTotalCost = (valueQuantity * dishCost) + entreeItemsCost

                val dishUpdate = OrderDishDto(
                    id = order?.id,
                    dishOrderName = order?.dishOrderName,
                    restaurantId = order?.restaurantId,
                    dishType = order?.dishType,
                    dishOrderSide = dishOrderDishSide,
                    dishOrderQuantity = valueQuantity,
                    dishOrderExtra = dishOrderEntree,
                    dishOrderSideFull = dishOrderFull,
                    dishTotalCost = dishTotalCost
                )

                updateOrderDishListener?.updateOrderDish(position,dishUpdate)
                dismiss()
            }

        }
    }


    private fun displayCurrentQuantity(value: Int) {
        value_quantity.text = value.toString()
    }
}