package com.hungrymedia.orderex.ui.dialog.orderEditDialog

import com.hungrymedia.orderex.dto.OrderDishDto

interface UpdateOrderDishListener {
    fun updateOrderDish(position: Int, dishUpdate: OrderDishDto)
}