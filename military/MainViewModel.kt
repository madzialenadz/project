package com.militaryconnection.android.view.activity.mainActivity

import androidx.lifecycle.MutableLiveData
import com.militaryconnection.android.database.UserPreferenceDao
import com.militaryconnection.android.di.BaseViewModel
import com.militaryconnection.android.view.session.SharePreferenceRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel : BaseViewModel() {

    @Inject
    lateinit var userDao: UserPreferenceDao

    @Inject
    lateinit var sessionPreferenceRepository: SharePreferenceRepository


    var userToken : MutableLiveData<String> = MutableLiveData()

    init {
        getUserData()
    }

    private fun getUserData() {
        userToken.postValue( sessionPreferenceRepository.getUserToken())
    }


    fun removeUser(){
        sessionPreferenceRepository.removeUserToken()
    }
}