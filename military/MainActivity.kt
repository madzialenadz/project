package com.militaryconnection.android.view.activity.mainActivity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.navigation.ui.AppBarConfiguration
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.militaryconnection.android.*
import kotlinx.android.synthetic.main.activity_main.*

import com.militaryconnection.android.api.post.PostResponse

import com.militaryconnection.android.view.SetTextView
import com.militaryconnection.android.view.activity.account.AccountActivity
import com.militaryconnection.android.view.activity.profile.ProfileType
import com.militaryconnection.android.view.activity.TravelActivity
import com.militaryconnection.android.view.activity.jobActivity.JobDetailActivity
import com.militaryconnection.android.view.activity.newsDetils.NewsDetailActivity
import com.militaryconnection.android.view.activity.profile.UserAccountActivity
import com.militaryconnection.android.view.activity.sceen.ScreenActivity
import com.militaryconnection.android.view.activity.sceen.ScreenType
import com.militaryconnection.android.view.communityNews.CommunityNewsFragment
import com.militaryconnection.android.view.dealsAndPromotions.DealsAndPromotionsFragment
import com.militaryconnection.android.view.dialog.SignRegisterDialogFragment
import com.militaryconnection.android.view.home.HomeFragment
import com.militaryconnection.android.view.home.list.latestJobs.JobsItem
import com.militaryconnection.android.view.jobPortal.JobPortalFragment

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var viewModel: MainViewModel

    private var isLogged: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)
        navView.setCheckedItem(R.id.nav_home)

        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        if (savedInstanceState == null) {
            showFragment(HomeFragment.newInstance())
        }

        viewModel.userToken.observe(this, Observer {

            if (it.isEmpty()) {
                isLogged = false

                navView.menu.findItem(R.id.nav_log_out).isVisible = false

                button_sign_in_main.visibility = View.VISIBLE
                button_sign_in_main.setOnClickListener {
                    val showCreateAccountType = SignRegisterDialogFragment.newInstance()
                    showCreateAccountType.show(this)
                }
                SetTextView.setTextDotHaveAccount(this, text_footer)

            } else {
                isLogged = true

                navView.menu.findItem(R.id.nav_log_out).isVisible = true
                button_sign_in_main.visibility = View.GONE
                text_footer.setText(R.string.copyright_2019_n_military)
            }

        })


    }


    fun navigateToTypeAccount(view: String) {
        val intent = Intent(this, AccountActivity::class.java)
        intent.putExtra(ACCOUNT_TYPE, view)
        startActivity(intent)
    }


    fun navigateToTravel() {
        val intent = Intent(this, TravelActivity::class.java)
        startActivity(intent)
    }

    fun navigationJobDetails(jobsItem: JobsItem,typJob:String){
        val intent = Intent(this, JobDetailActivity::class.java)
        intent.putExtra(JOB_DETAIL, jobsItem)
        intent.putExtra(JOB_TYPE,typJob)
        startActivity(intent)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                showFragment(HomeFragment.newInstance())
            }

            R.id.nav_deals_and_promotions -> {
                showFragment(DealsAndPromotionsFragment.newInstance())
            }

            R.id.nav_job_portal -> {
                showFragment(JobPortalFragment.newInstance())
            }

            R.id.nav_community_news -> {
                showFragment(CommunityNewsFragment.newInstance())
            }

            R.id.nav_applications -> {
                if (isLogged) {
                    val intent = Intent(this, ScreenActivity::class.java)
                    intent.putExtra(SCREEN_TYPE, ScreenType.APPLICATIONS.type)
                    startActivity(intent)
                }
            }

            R.id.nav_profile -> {
                if (isLogged) {
                    val intent = Intent(this, UserAccountActivity::class.java)
                    intent.putExtra(PROFILE_TYPE, ProfileType.PROFILE.type)
                    startActivity(intent)
                }
            }

            R.id.nav_about_us -> {
                val goToViewAboutApp = Intent(this, ScreenActivity::class.java)
                goToViewAboutApp.putExtra(SCREEN_TYPE, ScreenType.ABOUT_APP.type)
                startActivity(goToViewAboutApp)
            }

            R.id.nav_log_out -> {

                val startHome = Intent(this, MainActivity::class.java)
                startHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(startHome)
                viewModel.removeUser()
            }

        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.content_main, fragment)
            .commit()
    }


    fun navigateNewsDetils(postResponse: PostResponse){
        val intent = Intent(this, NewsDetailActivity::class.java)
        intent.putExtra(NEWS_DETAILS, postResponse)
        startActivity(intent)
    }

}
