

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import pl.ninebits.moviestonight.injection.base.BaseViewModel
import pl.ninebits.moviestonight.network.MovieApi
import pl.ninebits.moviestonight.session.SharePreferenceRepository
import pl.ninebits.moviestonight.utils.globalFunction.NetworkMessage
import pl.ninebits.moviestonight.view.movieDetils.MovieDetailValidation
import retrofit2.HttpException
import javax.inject.Inject

class MovieDetailViewModel : BaseViewModel() {

    var disposable: Disposable? = null

    @Inject
    lateinit var movieApi: MovieApi

    var contractView: MovieDetailValidation? = null

    @Inject
    lateinit var sessionPreferenceRepository: SharePreferenceRepository


    fun isUserLogged(): Boolean {
        return sessionPreferenceRepository.isTokenEmpty()
    }

    fun showMovieDetail(movieId: Long?) {
        disposable = movieApi.getMovieDetails(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                contractView?.showProgressMovie(false)
                contractView?.showMovieDetailSuccess(it.movieDetail)
            }, {
                if (it is HttpException) {
                    contractView?.showMovieDetailError(NetworkMessage.getErrorMessage(it.response().errorBody()))
                }
            }

            )
    }

    fun addMovieToList(typeList: String, movieId: Long?) {

        disposable = movieApi.addMovieToList(typeList, movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                contractView?.addMovieToListSuccess(it.message)
            }, {
                if (it is HttpException) {
                    contractView?.addMovieToListError(NetworkMessage.getErrorMessage(it.response().errorBody()))
                }
            })

    }

    fun deleteMoviesFromList(typeList: String, movieId: Long?) {
        disposable = movieApi.deleteMoviesFromList(typeList, movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                contractView?.deleteMovieToListSuccess(it.message)
            }, {
                if (it is HttpException) {
                    contractView?.deleteMovieToListError(NetworkMessage.getErrorMessage(it.response().errorBody()))
                }
            })
    }

}