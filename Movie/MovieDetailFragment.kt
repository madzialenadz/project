

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_movie_details.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pl.ninebits.moviestonight.R
import pl.ninebits.moviestonight.api.movieDetail.MovieDetails
import pl.ninebits.moviestonight.utils.extension.isShowView
import pl.ninebits.moviestonight.utils.extension.messageUserDontAccess
import pl.ninebits.moviestonight.utils.globalFunction.SetPicture
import pl.ninebits.moviestonight.utils.constans.value.ListMovieValue.LIST_MOVIE_FAVOURITES
import pl.ninebits.moviestonight.utils.constans.value.ListMovieValue.LIST_MOVIE_WATCHED
import pl.ninebits.moviestonight.utils.constans.value.MovieValue.MOVIE_ID
import pl.ninebits.moviestonight.view.base.fragment.BaseViewFragment
import pl.ninebits.moviestonight.view.dialogFragment.rateMovieDialogFragment.RateMovieDialogFragment
import pl.ninebits.moviestonight.view.movieDetils.MovieDetailValidation
import pl.ninebits.moviestonight.view.movieDetils.MovieDetailViewModel
import pl.ninebits.moviestonight.view.movieDetils.model.RateUserEvent
import pl.ninebits.moviestonight.view.toolbar.movie.ToolbarType
import pl.ninebits.moviestonight.view.toolbar.movie.toolbarMovieView.ToolbarDataEvent


class MovieDetailFragment : BaseViewFragment(), MovieDetailValidation, View.OnClickListener {

    private var movieId: Long? = null
    private lateinit var movieDetailViewModel: MovieDetailViewModel
    private var isUserLogged: Boolean = false
    private var userRating: Float = 0.0f

    companion object {
        fun newInstance(movieId: Long?): MovieDetailFragment {
            val fragment = MovieDetailFragment()
            val args = Bundle()
            movieId?.let {
                args.putLong(MOVIE_ID, movieId)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieId = arguments?.getLong(MOVIE_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_movie_details, container, false)
        movieDetailViewModel = ViewModelProviders.of(this).get(MovieDetailViewModel::class.java)
        movieDetailViewModel.contractView = this
        movieDetailViewModel.showMovieDetail(movieId)
        isUserLogged = movieDetailViewModel.isUserLogged()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClickLister()
    }

    override fun showMovieDetailSuccess(movieDetail: MovieDetails) {

        userRating = movieDetail.meta.userRating.toFloat() / 2.0f
        setRateUserView(userRating)

        fragment_movie_detlis_reated.text = String.format(getString(R.string.rating, movieDetail.voteCount.toString()))
        fragment_movie_details_movie_title.text = movieDetail.originalTitle
        fragment_movie_details_description.text = movieDetail.overview
        fragment_movie_detlis_popularity.text =
            String.format(getString(R.string.popularity, movieDetail.popularity.toString()))
        fragment_movie_detlis_runTime.text =
            String.format(getString(R.string.movie_time_run, formatHoursAndMinutes(movieDetail.runtime)))

        fragment_movie_detlis_is_faovorite.isChecked = movieDetail.meta.inFavourites
        fragment_movie_detalis_is_wached.isChecked = movieDetail.meta.inWatched

        fragment_movie_detlis_imdb.text = String.format(getString(R.string.imdb, movieDetail.voteAverage.toString()))

        val genreList: MutableList<String> = ArrayList()

        for (genre in movieDetail.genres) {
            genreList.add(genre.name)
        }

        val genreValue = genreList.joinToString(", ")

        fragment_movie_detlis_genre.text = genreValue
        context.let {
            SetPicture.setPosterMovie(
                it,
                movieDetail.posterPath,
                fragment_movie_details_poster
            )
        }


    }

    override fun onResume() {
        super.onResume()
        setToolbarView?.showToolbarView(ToolbarType.TOOLBAR_BASE.ordinal)

        EventBus.getDefault().postSticky(
            ToolbarDataEvent(
                R.string.movie_detail,
                isNavigationBack = true,
                isEmailSearchView = false
            )
        )

    }

    private fun setClickLister() {
        fragment_movie_detlis_is_faovorite.setOnClickListener(this)
        fragment_movie_detlis_add_list_movie.setOnClickListener(this)
        fragment_movie_detalis_is_wached.setOnClickListener(this)
        fragment_movie_detalis_rate_movie.setOnClickListener(this)
        fragment_movie_details_wached_text.setOnClickListener(this)
        fragment_rate_user.setOnClickListener(this)
        fragment_movie_detalis_rate_movie.setOnClickListener(this)
    }

    override fun showMovieDetailError(errorMessage: String?) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fragment_movie_detlis_is_faovorite -> {
                addRemoveMovieToList(v, LIST_MOVIE_FAVOURITES, movieId)
            }
            R.id.fragment_movie_detlis_add_list_movie -> {
                addFavoritesListMovieId()
            }

            R.id.fragment_movie_detalis_is_wached -> {
                addRemoveMovieToList(v, LIST_MOVIE_WATCHED, movieId)
            }

            R.id.fragment_rate_user -> {
                showRateMovieDialog()
            }
            R.id.fragment_movie_details_wached_text -> {
                fragment_movie_detalis_is_wached.isChecked = !fragment_movie_detalis_is_wached.isChecked
            }

        }
    }

    override fun addMovieToListSuccess(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun addMovieToListError(errorMessage: String?) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun deleteMovieToListSuccess(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun deleteMovieToListError(errorMessage: String?) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun setRateUserView(userRating: Float) {
        fragment_movie_detalis_rate_movie.isEnabled = true
        fragment_movie_detalis_rate_movie.rating = userRating
        fragment_rate_user.text = String.format(getString(R.string.ratingUser, userRating.toString()))
    }


    private fun showRateMovieDialog() {

        if (!isUserLogged) {
            val rateMovieDialogFragment = RateMovieDialogFragment.newInstance(movieId, userRating)
            rateMovieDialogFragment.setTargetFragment(this, 0)
            fragmentManager?.let {

                rateMovieDialogFragment.show(it, "watchingMovieTogether")
            }
        } else {
            messageUserDontAccess(context)
        }

    }

    private fun addFavoritesListMovieId() {
        if (!isUserLogged) {
            movieDetailViewModel.addMovieToList("favourites", movieId)
        } else {
            messageUserDontAccess(context)
        }
    }

    override fun showProgressMovie(isShow: Boolean) {
        fragment_movie_details_progress_bar.isShowView(isShow)
    }


    private fun formatHoursAndMinutes(totalMinutes: Int): String {

        val hours = totalMinutes / 60
        val minutes = totalMinutes % 60

        return if (hours > 0) {
            String.format(getString(R.string.full_time_movie, hours, minutes))
        } else {
            String.format(getString(R.string.short_time_movie, minutes))
        }

    }


    private fun addRemoveMovieToList(v: View, listType: String, movieId: Long?) {

        val checkBox = v as CheckBox

        if (!isUserLogged) {
            if (checkBox.isChecked) {
                movieDetailViewModel.addMovieToList(listType, movieId)
            } else {
                movieDetailViewModel.deleteMoviesFromList(listType, movieId)
            }
        } else {
            messageUserDontAccess(context)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    private fun setRatingMovie(rateUserEvent: RateUserEvent) {
        val (rateUserValue) = rateUserEvent
        fragment_rate_user.text = String.format(getString(R.string.ratingUser, rateUserValue.toString()))
    }

    override fun onDestroy() {
        super.onDestroy()
        movieDetailViewModel.contractView = null

    }
}