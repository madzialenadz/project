package pl.ninebits.moviestonight.view.movieDetils

import pl.ninebits.moviestonight.api.movieDetail.MovieDetails

interface MovieDetailValidation {
    fun showProgressMovie(isShow: Boolean)
    fun showMovieDetailSuccess(movieDetail: MovieDetails)
    fun showMovieDetailError(errorMessage: String?)
    fun addMovieToListSuccess(message: String?)
    fun addMovieToListError(errorMessage: String?)
    fun deleteMovieToListSuccess(message: String?)
    fun deleteMovieToListError(errorMessage: String?)
}